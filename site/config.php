<?php

/**
 * Site Configuration File
 *
 * This file overrides properties defined in main configuration
 * file for Caracal located in `units/config.php`.
 */

use Core\CSP;
use Core\Cache\Type as CacheType;

// document standard
define('_STANDARD', 'html5');
define('_TIMEZONE', 'America/New_York');

define('DEBUG', 1);
// define('SQL_DEBUG', 1);

// site language configuration
$available_languages = array('he', 'en');
$default_language = 'he';

// default session options
$session_type = Core\Session\Type::BROWSER;

// database
$db_type = DatabaseType::MYSQL;
$db_config = array(
		'host' => 'localhost',
		'user' => 'root',
		'pass' => 'caracal',
		'name' => 'web_engine'
	);

// allow loading scripts from different domain
/* CSP\Parser::add_value(CSP\Element::SCRIPTS, 'domain.com'); */

/*
CSP\Policy::add_values('script-src', array(
	'www.googletagmanager.com', 'www.google-analytics.com', 'googleads.g.doubleclick.net',
	'accessible.vegas.co.il', 'www.youtube.com', 'googleapis.com'
));

$domain = Manager::add_domain('https://play.google.com');
Manager::allow_methods($domain, array('GET', 'PUT'));
 */
CSP\Policy::set_disabled(true);  // current config on site, should be configured

// force secure connections
$force_https = true;

// configure code generation
$cache_method = Core\Cache\Type::NONE;
$optimize_code = false;
$include_styles = true;
$url_rewrite = true;

$closure_compiler_config = array(
	'hostname' => 'compiler.way2cu.com',
	'endpoint' => '/'
);
?>
