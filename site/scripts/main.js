/**
 * Main JavaScript
 * Yuni Site
 *
 * Copyright (c) 2015. by Way2CU, http://way2cu.com
 * Authors: Mladen Mijatov, Vladimir Anusic
 */

// create or use existing site scope
var Site = Site || {};

/**
 * Check if site is being displayed on mobile.
 * @return boolean
 */
Site.is_mobile = function() {
	let element = document.querySelector('meta[name=is-mobile]');
	return element.content == '1';
};

/**
 * Handle assigning token value to textarea value attribute so it is sent in form submit
 *
 * @param string token
 */
window.handle_captcha_response = function(token) {
	// custom made textarea to hold token value
	var textarea = $("#custom-area");
	textarea.val(token);
}

/**
 *	Handle toggling between image and image description
 *
 *	@param object event
 */
Site.handle_toggling = function(event) {
	event.preventDefault();
	var description_div = event.target.nextSibling.childNodes[1];

	if (description_div && !description_div.classList.contains('toggle')) {
		description_div.classList.add('toggle');
	} else {
		description_div.classList.remove('toggle');
	}

	if (event.target.classList.contains('toggle'))
		event.target.classList.remove('toggle');
};

/**
 *	Handle toggling description
 *
 *	@param object event
 */
Site.handle_toggling_description = function(event) {
	event.preventDefault();

	if (event.target.classList.contains('toggle'))
		event.target.classList.remove('toggle');
};

/**
 *	Handle visibility toggling on main menu
 *
 *	@param object event
 */
Site.handle_menu_toggling = function(event) {
	event.preventDefault();
	var header_menu = document.querySelector('div.menu > nav');
	header_menu.classList.toggle('input-focused');
};

/**
 * Function called when document and images have been completely loaded.
 */
Site.on_load = function() {
	//  if home page, crate PageControl object for news feeds
	if (window.location.pathname == '/') {

		// controls for moving through news feeds
		Site.news_controls = new PageControl('div#news', 'a.news');
		Site.news_controls
			.attachNextControl($('a.next'))
			.attachPreviousControl($('a.previous'))
			.setWrapAround(true)
			.setPauseOnHover(true)
			.setInterval(3000);
	}

	var is_our_team_page = window.location.href.indexOf('/our-team') > -1;

	if (!Site.is_mobile() && is_our_team_page) {
		Site.our_team_gallery = new Caracal.Gallery.Slider(4, false);
		Site.our_team_gallery
			.images.set_container('div#our_team_images_holder')
			.images.set_center(true)
			.images.set_spacing(40)
			.images.add('div#our_team_images_holder figure')
			.controls.attach_next(document.querySelector('div#our_team_images_holder a.next'))
			.controls.attach_previous(document.querySelector('div#our_team_images_holder a.previous'))
			.controls.set_pause_on_hover(true)
			.controls.set_auto(2000)
			.images.update();
	}

	// handle navigation visibility dependable on focus and blur events on search input
	var search_input = document.querySelector('div.menu form input');
	var search_submit_button = document.querySelector('div.menu form button[type=submit]');
	var menu_button = document.querySelector('#menu_button');
	var contact_link = document.querySelector('a.contact');

	search_input.addEventListener('focus', Site.handle_menu_toggling);
	search_input.addEventListener('blur', Site.handle_menu_toggling);
	search_submit_button.addEventListener('focus', Site.handle_menu_toggling);
	search_submit_button.addEventListener('blur', Site.handle_menu_toggling);

	// prevent menu from closing when contact link is focused
	contact_link.addEventListener('focus', Site.handle_menu_toggling);
	contact_link.addEventListener('blur', Site.handle_menu_toggling);

	if (Site.is_mobile()) {
		// image on our team page
		var images = document.querySelectorAll('figure.image img');
		var description_divs = document.querySelectorAll('figure.image figcaption div');

		for (var i = 0; i < images.length; i++) {
			images[i].addEventListener('touchend', Site.handle_toggling);
		}

		for (var i = 0; i < description_divs.length; i++) {
			description_divs[i].addEventListener('touchend', Site.handle_toggling_description);
		}
	}

	// create expanding menu
	var menu_items = document.querySelectorAll('div.menu > nav > a');
	for (var i=0, count=menu_items.length; i<count; i++) {
		if (menu_items[i].classList.contains('category')) {
			menu_items[i].addEventListener('click', function(event) {
				event.preventDefault();
				event.currentTarget.nextSibling.classList.toggle('active');
			});
			menu_items[i].addEventListener('keydown', function(event) {
				if (event.keyCode === 13) {
					event.preventDefault();
					event.currentTarget.nextSibling.classList.toggle('active');
				}
			});

			// prevent menu form closing when top level links have focus
			menu_items[i].addEventListener('focus', Site.handle_menu_toggling);
			menu_items[i].addEventListener('blur', Site.handle_menu_toggling);
		}
	}

	var second_level_menu = document.querySelectorAll('div.menu > nav > nav > a');
	for (var i=0, count=second_level_menu.length; i < count; i++) {
		second_level_menu[i].addEventListener('focus', Site.handle_menu_toggling);
		second_level_menu[i].addEventListener('blur', Site.handle_menu_toggling);
	}

	window.addEventListener('scroll', function(event) {
		if (window.scrollY > 50)
			document.querySelector('.mobile_title').classList.add('active'); else
			document.querySelector('.mobile_title').classList.remove('active');
	});
};

// connect document `load` event with handler function
window.addEventListener('load', Site.on_load);
